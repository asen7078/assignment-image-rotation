#include "bmp.h"
#include "image_rotation.h"


int main(int argc, char** argv) 
{
    if (argc < 3) 
    {
        return -1;
    }
    
    FILE* file_src = fopen(argv[1], "rb");  

    FILE* file_dst = fopen(argv[2], "wb");

    struct image image_src = { 0 };
    if (!load_bmp(file_src, &image_src)) 
    {
        return -1;
    }

    fclose(file_src);
    
    struct image image_dst = { 0 };

    effect_rotate_image(&image_src, &image_dst);
    destroy_image(&image_src);

    if (!save_bmp(file_dst, &image_dst))
    {
        fprintf(stderr, "Couldn't write file");
        return -1;
    }

    fclose(file_dst);
    destroy_image(&image_dst);
    destroy_image(&image_src);

    return 0;
}
