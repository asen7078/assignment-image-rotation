#include "bmp.h"
#include "util.h"

#define BMP_HEADER_BM 0x4D42
#define BFRESERVED1 0
#define BFRESERVED2 0
#define BISIZE 40
#define BIPLANES 1
#define BIBITCOUNT 24
#define BIXPELSPERMETER 0
#define BIYPELSPERMETER 0
#define BICLRUSED 0
#define BICLRIMPORTANT 0

#if defined(_MSC_VER)
#pragma pack(push, 1)
#define ALIGN_1
#else
#define ALIGN_1 __attribute__((packed))
#endif

enum bmp_pixel_type {
    BPT_RGB = 0,
    BPT_RLE8,   
    BPT_RLE4,    
    BPT_BITFIELDS, 
    BPT_JPEG,      
    BPT_PNG    
};


struct ALIGN_1 bmp_header {
    uint16_t bfType;  // Тип.    
    uint32_t bfSize;   // Размер.         
    uint16_t bfReserved1; // Зарезервирован.
    uint16_t bfReserved2; // Зарезервирован.
    uint32_t bfOffBits; // Положение пиксельных данных относительно начала данной структуры (в байтах).
    uint32_t biSize; // Размер данной структуры в байтах
    uint32_t biWidth; // Ширина растра в пикселях.
    uint32_t biHeight;// Высота.
    uint16_t biPlanes;// Высота растра в пикселях (абсолютное значение числа)
    uint16_t biBitCount;// Количество бит
    uint32_t biCompression;// Указывает на способ хранения пикселей.
    uint32_t biSizeImage; // Размер изображения.
    uint32_t biXPelsPerMeter; // Количество пикселей на метр по горизонтали.
    uint32_t biYPelsPerMeter;// Количество пикселей на метр повертикали.
    uint32_t biClrUsed;// Размер таблицы цветов в ячейках.
    uint32_t biClrImportant;// Количество ячеек от начала таблицы цветов до последней используемой.  
};

size_t calculate_padding (size_t line_size) {
    return 4 - (int32_t)(line_size % 4);
}

bool check_file(struct bmp_header file_header, struct image* out_image) {
    if (file_header.bfType != BMP_HEADER_BM ||
        file_header.biCompression != BPT_RGB ||
        !out_image->pixels)
    {
        return false;
    }

    return true;
}

bool save_bmp(FILE* file, const struct image* in_image) {
    assert(file);
    assert(in_image);

    size_t line_size = (size_t)in_image->width * 3;
    size_t remainder = line_size % 4;

    //size_t padded_line_size = line_size;
    size_t padded_line_size = calculate_padding(line_size); //??

    
    if (remainder) 
    {
        padded_line_size += 4 - (int32_t)remainder;
    }
    
    size_t image_size  = padded_line_size * in_image->height;
    size_t header_size = sizeof(struct bmp_header);
    size_t file_size   = header_size + image_size;

    struct bmp_header file_header;
    file_header.bfType          = BMP_HEADER_BM;
    file_header.bfSize          = (uint32_t)file_size;
    file_header.bfReserved1     = BFRESERVED1;
    file_header.bfReserved2     = BFRESERVED2;
    file_header.bfOffBits       = (uint32_t)header_size;
    file_header.biSize          = BISIZE; 
    file_header.biWidth         = in_image->width;
    file_header.biHeight        = in_image->height;
    file_header.biCompression   = BPT_RGB;
    file_header.biSizeImage     = (uint32_t)image_size;
    file_header.biPlanes        = BIPLANES;
    file_header.biBitCount      = BIBITCOUNT;
    file_header.biXPelsPerMeter = BIXPELSPERMETER;
    file_header.biYPelsPerMeter = BIYPELSPERMETER;
    file_header.biClrUsed       = BICLRUSED;
    file_header.biClrImportant  = BICLRIMPORTANT;

    if (file){
        fwrite(&file_header, sizeof(file_header), 1, file);
    }

    uint8_t* pixels = (uint8_t*)in_image->pixels;
    int32_t padding_size = calculate_padding(line_size);

    for (size_t i = 0; i < in_image->height; i += 1) 
    {
        fwrite(pixels + line_size * i, line_size, 1, file);

        char padding_to_write[4]={0};
        fwrite(&padding_to_write, padding_size, 1, file);
    }

    return true;
}


bool load_bmp(FILE* file, struct image* out_image) {
    assert(file);
    assert(out_image);

    // Уничтожаем картинку.
    destroy_image(out_image);

    fseek(file, 0, SEEK_END);
    // size_t file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    struct bmp_header file_header;

    // if (file_size < sizeof(file_header)) 
    // {
    //     return false;
    // }

    fread(&file_header, sizeof(file_header), 1, file);

    *out_image = allocate_image(file_header.biWidth, file_header.biHeight);

    if (!check_file(file_header, out_image))
    {
        return false;
    }
    if (file) {
        fseek(file, file_header.bfOffBits, SEEK_SET);
    }
    size_t line_size = (size_t)file_header.biWidth * 3;

    int32_t padding_size = 4 - (int32_t)(line_size % 4);
    
    for (size_t i = 0; i < (size_t)file_header.biHeight; i++) 
    {
        fread((uint8_t*)out_image->pixels + line_size * i, line_size, 1, file);
        fseek(file, padding_size, SEEK_CUR);
    }

    return true;
}
