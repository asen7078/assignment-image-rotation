#include "image.h"


void destroy_image(struct image* image) {
	assert(image);

	free(image->pixels);

	image->width  = 0;
	image->height = 0;
	image->pixels = NULL;
}

struct image allocate_image(uint32_t width, uint32_t height) {
	struct image _img;

	assert(height && "Height can't be equal 0");
	assert(width  && "Width can't be equal 0");

    _img.width  = width;
    _img.height = height;
    _img.pixels = malloc(get_image_size(&_img));
	
	return _img;
}

size_t get_image_size(const struct image* image) {
	return (size_t)image->height *  sizeof(struct pixel) * (size_t)image->width;
}
