#include "util.h"
#include "image_rotation.h"


void effect_rotate_image(const struct image* in_image, struct image* out_image) {
	assert(in_image);
	assert(out_image);

	destroy_image(out_image);

	*out_image = allocate_image(in_image->height, in_image->width);

	if (!out_image->pixels) 
	{
		return;
	}

	for (size_t i = 0; i < in_image->width; i++) 
	{
		for (size_t j = 0; j < in_image->height; j++) 
		{
			struct pixel* src_pixel = in_image->pixels  + (j * in_image->width + i);
			struct pixel* dst_pixel = out_image->pixels + ((i + 1) * in_image->height - 1 - j);

			*dst_pixel = *src_pixel;
		}
	}
}
