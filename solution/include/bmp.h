#pragma once
#include "util.h"
#include "image.h"


bool load_bmp(FILE* file, struct image* out_image);
bool save_bmp(FILE* file, const struct image* in_image);
size_t calculate_padding (size_t line_size);
// bool check_file(struct bmp_header file_header, struct image* out_image);
// struct bmp_header get_header(size_t padded_line_size, const struct image* in_image, size_t image_size, size_t header_size, size_t file_size);
