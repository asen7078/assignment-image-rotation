#pragma once
#include "util.h"


struct image {
	struct pixel* pixels;

	uint32_t width __attribute__((aligned (16)));
	uint32_t height __attribute__((aligned (16)));
};


struct pixel {
	uint8_t r, g, b;
};


struct image allocate_image(uint32_t width, uint32_t height);
void destroy_image(struct image* image);

size_t get_image_size(const struct image* image);

